# TheEvilSkeleton

Also known as TheEvilSkely, Skelly and Tesk.

Personal website: https://tesk.page

Where to find me:
- Codeberg: https://codeberg.org/TheEvilSkeleton
- GitHub: [TheEvilSkeleton](https://github.com/TheEvilSkeleton)
- \[matrix\]: [`@theevilskeleton:fedora.im`](https://matrix.to/#/@theevilskeleton:fedora.im)
- Mastodon: [`@TheEvilSkeleton@fosstodon.org`](https://fosstodon.org/@TheEvilSkeleton)

---

## Projects I work on

### Personal Projects
- [vkbasalt-cli](https://gitlab.com/TheEvilSkeleton/vkbasalt-cli)

### Collaborative projects
- [Bottles](https://github.com/bottlesdevs/Bottles)
- [Upscaler](https://gitlab.com/TheEvilSkeleton/Upscaler)

